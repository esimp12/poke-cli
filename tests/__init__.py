import requests

from pokemon.api import PokeAPI


global POKE_API
POKE_API = PokeAPI()

BAD_URL_1 = f"{POKE_API.rest_api}/pokemon/BAD"
BAD_URL_2 = f"{POKE_API.rest_api}/type/BAD"

GOOD_POKEMON = 'pikachu'
GOOD_POKEDEX = 25
GET_GOOD_POKEMON_URL = f"{POKE_API.rest_api}/pokemon/{GOOD_POKEMON}"
GET_GOOD_POKEDEX_URL = f"{POKE_API.rest_api}/pokemon/{GOOD_POKEDEX}"
GET_GOOD_POKE_INFO_RES = {
    'id': GOOD_POKEDEX,
    'name': GOOD_POKEMON,
    'moves': [
        {
            'move': {'name': 'mega-punch'},
            'version_group_details': [
                {'version_group': {'name': 'red-blue'}},
                {'version_group': {'name': 'yellow'}}
            ]
        },
        {
            'move': {'name': 'skull-bash'},
            'version_group_details': [
                {'version_group': {'name': 'red-blue'}}
            ]
        }
    ]
}

BAD_POKEMON = 'pika'
BAD_POKEDEX = 10000
GET_BAD_POKEMON_URL = f"{POKE_API.rest_api}/pokemon/{BAD_POKEMON}"
GET_BAD_POKEDEX_URL = f"{POKE_API.rest_api}/pokemon/{BAD_POKEDEX}"

GOOD_MOVE_TYPE = 'normal'
GOOD_MOVE_TYPE_ID = 1
GET_GOOD_MOVE_TYPE_URL = f"{POKE_API.rest_api}/type/{GOOD_MOVE_TYPE}"
GET_GOOD_MOVE_TYPE_ID_URL = f"{POKE_API.rest_api}/type/{GOOD_MOVE_TYPE_ID}"

POUND_MOVE = 'pound'
POUND_MOVE_URL = f"{POKE_API.rest_api}/move/1/"
SLAP_MOVE = 'double-slap'
SLAP_MOVE_URL = f"{POKE_API.rest_api}/move/3/"
GEN_1_URL = f"{POKE_API.rest_api}/generation/1/"
GEN_2_URL = f"{POKE_API.rest_api}/generation/2/"
POUND_MOVE_INFO_RES = {
    'generation': {
        'url': GEN_1_URL
    }
}
SLAP_MOVE_INFO_RES = {
    'generation': {
        'url': GEN_2_URL
    }
}
GENERATION_1_INFO_RES = {
    'version_groups': [
        {'name': 'red-blue'},
        {'name': 'yellow'}
    ]
}
GENERATION_2_INFO_RES = {
    'version_groups': [
        {'name': 'red-blue'}
    ]
}

GET_GOOD_MOVE_TYPE_INFO_RES = {
    'id': GOOD_MOVE_TYPE_ID,
    'name': GOOD_MOVE_TYPE,
    'moves': [
        {
            'name': POUND_MOVE,
            'url': POUND_MOVE_URL
        },
        {
            'name': SLAP_MOVE,
            'url': SLAP_MOVE_URL
        }
    ]
}


BAD_MOVE_TYPE = 'norm'
BAD_MOVE_TYPE_ID = 10000
GET_BAD_MOVE_TYPE_URL = f"{POKE_API.rest_api}/type/{BAD_MOVE_TYPE}"
GET_BAD_MOVE_TYPE_ID_URL = f"{POKE_API.rest_api}/type/{BAD_MOVE_TYPE_ID}"

GOOD_FILTER = 'yellow'
BAD_FILTER = 'none'


# Mock GET requests
def mock_requests_get(*args, **kwargs):
    class MockResponse:
        def __init__(self, json, status):
            self._json = json
            self._status = status

        def raise_for_status(self):
            if 400 <= self._status < 500:
                raise requests.exceptions.HTTPError('', response=self)
            elif 500 <= self._status < 600:
                raise requests.exceptions.RequestException('', response=self)

        def json(self):
            return self._json

    # Mock response for generic error (timeout, connection, etc.)
    if args[0] in (BAD_URL_1, BAD_URL_2):
        return MockResponse(None, 500)

    # Mock response for getting good pokemon
    elif args[0] in (GET_GOOD_POKEMON_URL, GET_GOOD_POKEDEX_URL):
        return MockResponse(GET_GOOD_POKE_INFO_RES, 200)

    # Mock response for getting bad pokemon
    elif args[0] in (GET_BAD_POKEMON_URL, GET_BAD_POKEDEX_URL):
        return MockResponse(None, 404)

    # Mock response for getting good move type
    elif args[0] in (GET_GOOD_MOVE_TYPE_URL, GET_GOOD_MOVE_TYPE_ID_URL):
        return MockResponse(GET_GOOD_MOVE_TYPE_INFO_RES, 200)

    # Mock response for getting bad move type
    elif args[0] in (GET_BAD_MOVE_TYPE_URL, GET_BAD_MOVE_TYPE_ID_URL):
        return MockResponse(None, 404)

    # Mock intermediate responses for getting move type generations
    elif args[0] == POUND_MOVE_URL:
        return MockResponse(POUND_MOVE_INFO_RES, 200)

    elif args[0] == SLAP_MOVE_URL:
        return MockResponse(SLAP_MOVE_INFO_RES, 200)

    elif args[0] == GEN_1_URL:
        return MockResponse(GENERATION_1_INFO_RES, 200)

    elif args[0] == GEN_2_URL:
        return MockResponse(GENERATION_2_INFO_RES, 200)