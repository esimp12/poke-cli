import unittest
from unittest.mock import patch, call

from pokemon.pokemon import cli
from pokemon.api import PokeError


class TestPokemon(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        # Command line args to trigger
        cls.GOOD_LOOKUP_NO_FILTER_ARGS = ["pokemon", "--lookup", "pikachu"]
        cls.GOOD_LOOKUP_FILTER_ARGS = ["pokemon", "--lookup", "pikachu", "--generation", "red-blue"]
        cls.GOOD_MOVE_TYPE_NO_FILTER_ARGS = ["pokemon", "--move-type", "normal"]
        cls.GOOD_MOVE_TYPE_FILTER_ARGS = ["pokemon", "--move-type", "normal", "--generation", "red-blue"]
        
        # Mock responses and stdout
        cls.GOOD_LOOKUP_NO_FILTER_RES = {
            'pokemon': 'pikachu',
            'pokedex_number': 25,
            'moves': [
                {'name': 'pound', 'generations': ['yellow', 'red-blue']},
                {'name': 'skull-bash', 'generations': ['yellow']}
            ]
        }
        cls.GOOD_LOOKUP_NO_FILTER_STDOUT = [
            call("POKEMON:        pikachu"),
            call("POKEDEX NUMBER:      25"),
            call("-----------------------"),
            call(),
            call("2 MOVE(S):\n=========="),
            call("\tpound      - (yellow, red-blue)"),
            call("\tskull-bash - (yellow)")
        ]
        cls.GOOD_LOOKUP_FILTER_RES = {
            'pokemon': 'pikachu',
            'pokedex_number': 25,
            'moves': [
                {'name': 'pound', 'generations': ['yellow', 'red-blue']}
            ]
        }
        cls.GOOD_LOOKUP_FILTER_STDOUT = [
            call("POKEMON:        pikachu"),
            call("POKEDEX NUMBER:      25"),
            call("-----------------------"),
            call(),
            call("1 MOVE(S):\n=========="),
            call("\tpound - (yellow, red-blue)")
        ]

        cls.GOOD_MOVE_TYPE_NO_FILTER_RES = {
            'move_type': 'normal',
            'move_type_id': 25,
            'moves': [
                {'name': 'pound', 'generations': ['yellow', 'red-blue']},
                {'name': 'skull-bash', 'generations': ['yellow']}
            ]
        }
        cls.GOOD_MOVE_TYPE_NO_FILTER_STDOUT = [
            call("MOVE TYPE:    normal"),
            call("MOVE TYPE ID:     25"),
            call("--------------------"),
            call(),
            call("2 MOVE(S):\n=========="),
            call("\tpound      - (yellow, red-blue)"),
            call("\tskull-bash - (yellow)")
        ]
        cls.GOOD_MOVE_TYPE_FILTER_RES = {
            'move_type': 'normal',
            'move_type_id': 25,
            'moves': [
                {'name': 'pound', 'generations': ['yellow', 'red-blue']}
            ]
        }
        cls.GOOD_MOVE_TYPE_FILTER_STDOUT = [
            call("MOVE TYPE:    normal"),
            call("MOVE TYPE ID:     25"),
            call("--------------------"),
            call(),
            call("1 MOVE(S):\n=========="),
            call("\tpound - (yellow, red-blue)")
        ]

    @patch('builtins.print')
    @patch('pokemon.api.PokeAPI.get_pokemon_info')
    def test_lookup_good_no_filter(self, 
                                   mock_get_pokemon_info, 
                                   mock_print):
        mock_get_pokemon_info.return_value = self.GOOD_LOOKUP_NO_FILTER_RES

        with patch('sys.argv', self.GOOD_LOOKUP_NO_FILTER_ARGS):
            cli()
        self.assertListEqual(mock_print.mock_calls,
                             self.GOOD_LOOKUP_NO_FILTER_STDOUT)

    @patch('builtins.print')
    @patch('pokemon.api.PokeAPI.get_pokemon_info')
    def test_lookup_good_filter(self,
                                mock_get_pokemon_info,
                                mock_print):
        mock_get_pokemon_info.return_value = self.GOOD_LOOKUP_FILTER_RES

        with patch('sys.argv', self.GOOD_LOOKUP_FILTER_ARGS):
            cli()
        self.assertListEqual(mock_print.mock_calls,
                             self.GOOD_LOOKUP_FILTER_STDOUT)

    @patch('builtins.print')
    @patch('pokemon.api.PokeAPI.get_move_type_info')
    def test_move_type_good_no_filter(self,
                                      mock_get_move_type_info,
                                      mock_print):
        mock_get_move_type_info.return_value = self.GOOD_MOVE_TYPE_NO_FILTER_RES

        with patch('sys.argv', self.GOOD_MOVE_TYPE_NO_FILTER_ARGS):
            cli()
        self.assertListEqual(mock_print.mock_calls,
                             self.GOOD_MOVE_TYPE_NO_FILTER_STDOUT)

    @patch('builtins.print')
    @patch('pokemon.api.PokeAPI.get_move_type_info')
    def test_move_type_good_filter(self,
                                   mock_get_move_type_info,
                                   mock_print):
        mock_get_move_type_info.return_value = self.GOOD_MOVE_TYPE_FILTER_RES

        with patch('sys.argv', self.GOOD_MOVE_TYPE_FILTER_ARGS):
            cli()
        self.assertListEqual(mock_print.mock_calls,
                             self.GOOD_MOVE_TYPE_FILTER_STDOUT)

    @patch('builtins.print')
    @patch('pokemon.api.PokeAPI.get_move_type_info')
    def test_poke_error(self,
                        mock_get_move_type_info,
                        mock_print):
        mock_get_move_type_info.side_effect = PokeError("TEST")

        with patch('sys.argv', ["pokemon", "--move-type", "norm"]):
            cli()
        self.assertListEqual(mock_print.mock_calls,
                             [call("PokeError: TEST")])

    @patch('sys.stdout.write')
    def test_no_args(self, mock_sys_stdout_write):
        with patch('sys.argv', ["pokemon"]):
            cli()
        help_msg = 'usage: pokemon [-h] [--lookup LOOKUP] [--move-type MOVE_TYPE]\n'
        help_msg += '               [--generation GENERATION] [--version]\n'
        help_msg += '\noptional arguments:\n  -h, --help            show this help message and exit\n'
        help_msg += '  --lookup LOOKUP       pokemon name OR pokedex number to query moves for\n'
        help_msg += '  --move-type MOVE_TYPE\n                        move-type OR move-type'
        help_msg += ' id to query moves for\n  --generation GENERATION\n'
        help_msg += '                        generation to filter results by\n'
        help_msg += '  --version             version of PokeCLI program\n'
        self.assertListEqual(mock_sys_stdout_write.mock_calls,
                             [call(help_msg)])
