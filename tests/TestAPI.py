import unittest
from unittest.mock import patch

from requests.exceptions import RequestException

from pokemon.api import PokeError
from tests import POKE_API
from tests import BAD_URL_1, BAD_URL_2, GET_GOOD_POKEMON_URL
from tests import GOOD_POKEMON, GOOD_POKEDEX, BAD_POKEMON, BAD_POKEDEX
from tests import GOOD_MOVE_TYPE, GOOD_MOVE_TYPE_ID, BAD_MOVE_TYPE, BAD_MOVE_TYPE_ID
from tests import GOOD_FILTER, BAD_FILTER
from tests import GET_GOOD_POKE_INFO_RES, GET_GOOD_MOVE_TYPE_INFO_RES
from tests import GENERATION_1_INFO_RES, POUND_MOVE_INFO_RES
from tests import mock_requests_get


class TestAPI(unittest.TestCase):

    @classmethod
    def setUpClass(cls):
        global POKE_API
        cls.POKE_API = POKE_API
        cls.GOOD_POKE_INFO = {
            'pokemon': GOOD_POKEMON,
            'pokedex_number': GOOD_POKEDEX,
            'moves': [
                {'name': 'mega-punch', 'generations': ['red-blue', 'yellow']},
                {'name': 'skull-bash', 'generations': ['red-blue']}
            ]
        }
        cls.GOOD_MOVE_TYPE_INFO = {
            'move_type': GOOD_MOVE_TYPE,
            'move_type_id': GOOD_MOVE_TYPE_ID,
            'moves': [
                {'name': 'pound', 'generations': ['red-blue', 'yellow']},
                {'name': 'double-slap', 'generations': ['red-blue']}
            ]
        }

    def test_poke_error_repr(self):
        # Test with no msg
        err_no_msg = PokeError()
        self.assertEqual(repr(err_no_msg), "PokeError")

        # Test with msg
        err_msg = PokeError("TEST")
        self.assertEqual(repr(err_msg), "PokeError: TEST")

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_requests_exc(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            poke_info = self.POKE_API.get_pokemon_info('BAD')
        exc = ctx.exception
        self.assertEqual(repr(exc),
                         f"PokeError: {PokeError.UNEXPECTED_ERROR}RequestException('')")

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokemon_no_filter(self, mock_get):
        poke_info = self.POKE_API.get_pokemon_info(GOOD_POKEMON)
        self._get_pokemon_info_no_filter_test_helper(poke_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokedex_no_filter(self, mock_get):
        poke_info = self.POKE_API.get_pokemon_info(GOOD_POKEDEX)
        self._get_pokemon_info_no_filter_test_helper(poke_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_bad_pokemon_no_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_pokemon_info(BAD_POKEMON)
        exc = ctx.exception
        err_msg = f"PokeError: '{BAD_POKEMON}' is not a valid pokemon!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_bad_pokedex_no_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_pokemon_info(BAD_POKEDEX)
        exc = ctx.exception
        err_msg = f"PokeError: '{BAD_POKEDEX}' is not a valid pokedex number!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokemon_good_filter(self, mock_get):
        poke_info = self.POKE_API.get_pokemon_info(GOOD_POKEMON, version_group=GOOD_FILTER)
        self._get_pokemon_info_filter_test_helper(poke_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokedex_good_filter(self, mock_get):
        poke_info = self.POKE_API.get_pokemon_info(GOOD_POKEDEX, version_group=GOOD_FILTER)
        self._get_pokemon_info_filter_test_helper(poke_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokemon_bad_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_pokemon_info(GOOD_POKEMON, version_group=BAD_FILTER)
        exc = ctx.exception
        err_msg = f"PokeError: '({GOOD_POKEMON} - {GOOD_POKEDEX})' does not appear in generation '{BAD_FILTER}'!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_pokemon_info_good_pokedex_bad_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_pokemon_info(GOOD_POKEDEX, version_group=BAD_FILTER)
        exc = ctx.exception
        err_msg = f"PokeError: '({GOOD_POKEMON} - {GOOD_POKEDEX})' does not appear in generation '{BAD_FILTER}'!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_requests_exc(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            move_type_info = self.POKE_API.get_move_type_info('BAD')
        exc = ctx.exception
        self.assertEqual(repr(exc),
                         f"PokeError: {PokeError.UNEXPECTED_ERROR}RequestException('')")

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_no_filter(self, mock_get):
        move_type_info = self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE)
        self._get_move_type_info_no_filter_test_helper(move_type_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_id_no_filter(self, mock_get):
        move_type_info = self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE_ID)
        self._get_move_type_info_no_filter_test_helper(move_type_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_bad_move_type_no_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_move_type_info(BAD_MOVE_TYPE)
        exc = ctx.exception
        err_msg = f"PokeError: '{BAD_MOVE_TYPE}' is not a valid move type!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_bad_move_type_id_no_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_move_type_info(BAD_MOVE_TYPE_ID)
        exc = ctx.exception
        err_msg = f"PokeError: '{BAD_MOVE_TYPE_ID}' is not a valid move type id!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_good_filter(self, mock_get):
        move_type_info = self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE, version_group=GOOD_FILTER)
        self._get_move_type_info_filter_test_helper(move_type_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_id_good_filter(self, mock_get):
        move_type_info = self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE_ID, version_group=GOOD_FILTER)
        self._get_move_type_info_filter_test_helper(move_type_info)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_bad_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE, version_group=BAD_FILTER)
        exc = ctx.exception
        err_msg = f"PokeError: '({GOOD_MOVE_TYPE} - {GOOD_MOVE_TYPE_ID})' does not appear in generation '{BAD_FILTER}'!"
        self.assertEqual(repr(exc), err_msg)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_move_type_info_good_move_type_id_bad_filter(self, mock_get):
        with self.assertRaises(PokeError) as ctx:
            self.POKE_API.get_move_type_info(GOOD_MOVE_TYPE_ID, version_group=BAD_FILTER)
        exc = ctx.exception
        err_msg = f"PokeError: '({GOOD_MOVE_TYPE} - {GOOD_MOVE_TYPE_ID})' does not appear in generation '{BAD_FILTER}'!"
        self.assertEqual(repr(exc), err_msg)

    def test_moves_handler(self):
        # Test moves handler without filtering by generation
        moves = POKE_API._moves_handler(GET_GOOD_POKE_INFO_RES)

        actual_move_names = []
        actual_move_generations = []
        for mv in moves:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = [mv['name'] 
                               for mv in self.GOOD_POKE_INFO['moves']]
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [mv['generations'] 
                                     for mv in self.GOOD_POKE_INFO['moves']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

        # Test moves handler with filtering by generation
        moves = POKE_API._moves_handler(GET_GOOD_POKE_INFO_RES, version_group=GOOD_FILTER)

        actual_move_names = []
        actual_move_generations = []
        for mv in moves:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = ['mega-punch']
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [['yellow', 'red-blue']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_move_type_moves_handler(self, mock_get):
        # Test moves handler without filtering by generation
        moves = POKE_API._move_type_moves_handler(GET_GOOD_MOVE_TYPE_INFO_RES)

        actual_move_names = []
        actual_move_generations = []
        for mv in moves:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = [mv['name'] 
                               for mv in self.GOOD_MOVE_TYPE_INFO['moves']]
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [mv['generations'] 
                                     for mv in self.GOOD_MOVE_TYPE_INFO['moves']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

        # Test moves handler with filtering by generation
        moves = POKE_API._move_type_moves_handler(GET_GOOD_MOVE_TYPE_INFO_RES, 
                                                  version_group=GOOD_FILTER)

        actual_move_names = []
        actual_move_generations = []
        for mv in moves:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = ['pound']
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [['yellow', 'red-blue']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_move_generation_handler(self, mock_get):
        gens = POKE_API._move_generation_handler(POUND_MOVE_INFO_RES)
        expected_gens = [vg['name'] 
                         for vg in GENERATION_1_INFO_RES['version_groups']]
        self.assertCountEqual(list(gens), expected_gens)

    @patch('requests.get', side_effect=mock_requests_get)
    def test_get_api_resource(self, mock_get):
        with self.assertRaises(RequestException) as ctx:
            data =  POKE_API._get_api_resource(BAD_URL_1)
        data = POKE_API._get_api_resource(GET_GOOD_POKEMON_URL)
        self.assertIsInstance(data, dict)
        self.assertDictEqual(data, GET_GOOD_POKE_INFO_RES)

    def _get_pokemon_info_no_filter_test_helper(self, poke_info):
        self.assertIsInstance(poke_info, dict)
        self.assertIn('pokemon', poke_info)
        self.assertIn('pokedex_number', poke_info)
        self.assertIn('moves', poke_info)
        # NOTE: Moves is a generator so we dont test dict equality
        # but check each expanded field is equal
        self.assertEqual(poke_info['pokemon'], self.GOOD_POKE_INFO['pokemon'])
        self.assertEqual(poke_info['pokedex_number'], 
                         self.GOOD_POKE_INFO['pokedex_number'])

        actual_move_names = []
        actual_move_generations = []
        for mv in poke_info['moves']:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = [mv['name'] 
                               for mv in self.GOOD_POKE_INFO['moves']]
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [mv['generations'] 
                                     for mv in self.GOOD_POKE_INFO['moves']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

    def _get_pokemon_info_filter_test_helper(self, poke_info):
        self.assertIsInstance(poke_info, dict)
        self.assertIn('pokemon', poke_info)
        self.assertIn('pokedex_number', poke_info)
        self.assertIn('moves', poke_info)
        # NOTE: Moves is a generator so we dont test dict equality
        # but check each expanded field is equal
        self.assertEqual(poke_info['pokemon'], self.GOOD_POKE_INFO['pokemon'])
        self.assertEqual(poke_info['pokedex_number'], 
                         self.GOOD_POKE_INFO['pokedex_number'])

        actual_move_names = []
        actual_move_generations = []
        for mv in poke_info['moves']:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = ['mega-punch']
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [['yellow', 'red-blue']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

    def _get_move_type_info_no_filter_test_helper(self, move_type_info):
        self.assertIsInstance(move_type_info, dict)
        self.assertIn('move_type', move_type_info)
        self.assertIn('move_type_id', move_type_info)
        self.assertIn('moves', move_type_info)
        # NOTE: Moves may be a generator so we dont test dict equality
        # but check each expanded field is equal
        self.assertEqual(move_type_info['move_type'], self.GOOD_MOVE_TYPE_INFO['move_type'])
        self.assertEqual(move_type_info['move_type_id'], 
                         self.GOOD_MOVE_TYPE_INFO['move_type_id'])

        actual_move_names = []
        actual_move_generations = []
        for mv in move_type_info['moves']:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = [mv['name'] 
                               for mv in self.GOOD_MOVE_TYPE_INFO['moves']]
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [mv['generations'] 
                                     for mv in self.GOOD_MOVE_TYPE_INFO['moves']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)

    def _get_move_type_info_filter_test_helper(self, move_type_info):
        self.assertIsInstance(move_type_info, dict)
        self.assertIn('move_type', move_type_info)
        self.assertIn('move_type_id', move_type_info)
        self.assertIn('moves', move_type_info)
        # NOTE: Moves may be a generator so we dont test dict equality
        # but check each expanded field is equal
        self.assertEqual(move_type_info['move_type'], self.GOOD_MOVE_TYPE_INFO['move_type'])
        self.assertEqual(move_type_info['move_type_id'], 
                         self.GOOD_MOVE_TYPE_INFO['move_type_id'])

        actual_move_names = []
        actual_move_generations = []
        for mv in move_type_info['moves']:
            actual_move_names.append(mv['name'])
            actual_move_generations.append(mv['generations'])

        # Check move names are the same
        expected_move_names = ['pound']
        self.assertCountEqual(actual_move_names, expected_move_names)

        # Check move generations
        expected_move_generations = [['yellow', 'red-blue']]
        for actual, expected in zip(actual_move_generations, 
                                    expected_move_generations):
            self.assertCountEqual(actual, expected)
