#!/bin/bash

echo "pokemon --lookup pikachu"
pokemon --lookup pikachu
echo

echo "pokemon --lookup 25"
pokemon --lookup 25
echo

echo "pokemon --lookup pikachu --generation yellow"
pokemon --lookup pikachu --generation yellow
echo

echo "pokemon --lookup 10000"
pokemon --lookup 10000
echo

echo "pokemon --lookup pika"
pokemon --lookup pika
echo

echo "pokemon --lookup lugia --generation yellow"
pokemon --lookup lugia --generation yellow
echo

echo "pokemon --move-type normal"
pokemon --move-type normal
echo

echo "pokemon --move-type norm"
pokemon --move-type norm
echo

echo "pokemon --move-type normal --generation yellow"
pokemon --move-type normal --generation yellow
echo
