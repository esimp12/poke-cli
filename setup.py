import re

from setuptools import setup

with open("src/pokemon/__init__.py", encoding="utf8") as f:
    version = re.search(r'__version__ = "(.*?)"', f.read()).group(1)

setup(
    name="pokemon",
    version=version,
    install_requires=[
        "requests>=2.25.0"
    ],
    test_suite='tests'
)
