PokeCLI
=======

Pokemon API command-line tool

HOW TO BUILD FROM SOURCE
========================

1. Clone repository ``git clone https://gitlab.com/esimp12/poke-cli.git``

2. ``cd poke-cli``

3. Build source ``python setup.py sdist bdist_wheel``

This built source wheel will get created locally in ``dist/pokemon-0.1-py3-none-any.whl``.

HOW TO INSTALL FROM SOURCE
==========================

1. Build source wheel

2. Install with pip ``pip install dist/pokemon-0.1-py3-none-any.whl``

HOW TO RUN
==========

The ``pokemon`` CLI program can be run either as a package or with
the pokemon script.

e.g. ``pokemon --lookup pikachu``

e.g. ``python -m pokemon --lookup pikachu``

HOW TO TEST
===========

1. Clone repository ``git clone https://gitlab.com/esimp12/poke-cli.git``

2. ``cd poke-cli``

3. Activate ``virtualenv`` Note: ``virtualenv`` and ``tox`` are required to test.

4. Run tests ``tox``.
