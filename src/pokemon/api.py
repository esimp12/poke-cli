import requests


class PokeError(Exception):
    """Custom exception instance for any errors encountered interfacing
    the poke api.

    :param `msg` - message for what error was encountered
    """

    UNEXPECTED_ERROR = "Pokemon CLI encountered an unexpected error => "

    def __init__(self, msg=None):
        super(PokeError, self).__init__()
        self._msg = msg

    def __repr__(self):
        cls_name = type(self).__name__
        return f"{cls_name}: {self._msg}" if self._msg else f"{cls_name}"


class PokeAPI:
    """Wrapper library for accessing and interfacing the pokmeon REST API.
    Supports querying pokemon, pokedex numbers, and move types as well as
    filtering by generations.

    :param `rest_api` - the rest api url to access
    :param `api` - the api version to use
    """

    def __init__(self, url='https://pokeapi.co/api', api='v2'):
        self.rest_api = f"{url}/{api}"

    def get_pokemon_info(self, id_or_name, **kwargs):
        """Retrieves pokemon info for a given pokemon name or pokedex
        number. Can supply filter arguments to parse results (e.g
        version_group=yellow).

        :param `id_or_name` - pokemon name or pokedex number to query
        :param `kwargs` - keyword arguments to pass to filter results
        :rtype dict
        :return dict of pokemon info
        """
        # Attempt to get pokemon info
        try:
            url = f"{self.rest_api}/pokemon/{id_or_name}"
            res = self._get_api_resource(url)
        # Handle unsuccessful pokemon/pokedex number queries
        except requests.exceptions.HTTPError:
            err_msg = f"'{id_or_name}' is not a valid pokemon!"
            if isinstance(id_or_name, int):
                err_msg = f"'{id_or_name}' is not a valid pokedex number!"
            raise PokeError(err_msg)
        # Handle any unexpected errors
        except requests.exceptions.RequestException as err:
            raise PokeError(f"{PokeError.UNEXPECTED_ERROR}{repr(err)}")

        # Parse request results
        pokemon = res['name']
        pokedex_number = res['id']
        moves = list(self._moves_handler(res, **kwargs))

        # Handle error where pokemon does not exist in generation
        if len(moves) == 0 and 'version_group' in kwargs:
            version_group = kwargs.get('version_group')
            err_msg = f"'({pokemon} - {pokedex_number})'"
            err_msg += f" does not appear in generation '{version_group}'!"
            raise PokeError(err_msg)

        poke_info = {
            'pokemon': pokemon,
            'pokedex_number': pokedex_number,
            'moves': moves
        }
        return poke_info

    def get_move_type_info(self, id_or_name, **kwargs):
        """Retrieves move type info for a given move type or move type
        id. Can supply filter arguments to parse results (e.g
        version_group=yellow).

        :param `id_or_name` - move type or move type id to query
        :param `kwargs` - keyword arguments to pass to filter results
        :rtype dict
        :return dict of move type info
        """
        # Attempt to get move type
        try:
            url = f"{self.rest_api}/type/{id_or_name}"
            res = self._get_api_resource(url)
        # Handle unsuccessful move type/move type id number queries
        except requests.exceptions.HTTPError:
            err_msg = f"'{id_or_name}' is not a valid move type!"
            if isinstance(id_or_name, int):
                err_msg = f"'{id_or_name}' is not a valid move type id!"
            raise PokeError(err_msg)
        # Handle any unexpected errors
        except requests.exceptions.RequestException as err:
            raise PokeError(f"{PokeError.UNEXPECTED_ERROR}{repr(err)}")

        # Parse results
        move_type = res['name']
        move_type_id = res['id']
        moves = list(self._move_type_moves_handler(res, **kwargs))

        # Handle error where move type moves do not exist in generation
        if len(moves) == 0 and 'version_group' in kwargs:
            version_group = kwargs.get('version_group')
            err_msg = f"'({move_type} - {move_type_id})'"
            err_msg += f" does not appear in generation '{version_group}'!"
            raise PokeError(err_msg)

        move_info = {
            'move_type': move_type,
            'move_type_id': move_type_id,
            'moves': moves
        }
        return move_info

    def _moves_handler(self, res, **kwargs):
        """Parses moves results and filters on any supplied queries.
        Yields a dict of the move name and any associated generations.

        :param `res` - the moves result to parse
        :param `kwargs` - keyword arguments to filter results by
        :rtype dict
        :return generator of move info
        """
        for move in res['moves']:
            name = move['move']['name']
            # NOTE: Generations from moves may have repeats due to multiple
            # learn methods, build as set and convert to list
            generations = {vg['version_group']['name']
                           for vg in move['version_group_details']}
            mv = {'name': name, 'generations': list(generations)}

            # Filter on generation if query field applied
            version_group = kwargs.get('version_group')
            if version_group:
                if version_group in mv['generations']:
                    yield mv
            else:
                yield mv

    def _move_type_moves_handler(self, res, **kwargs):
        """Parses moves results for move types and filters on any supplied
        queries. Yields a dict of the move name and any associated
        generations.

        :param `res` - the moves result to parse
        :param `kwargs` - keyword arguments to filter results by
        :rtype dict
        :return generator of move info
        """
        for move in res['moves']:
            name = move['name']
            url = move['url']
            # NOTE: Generations for moves associated with move types are
            # handled by 1) retrieving the move resource 2) retrieving
            # the accompyaning generation resource 3) and finally getting
            # the associated version groups
            res = self._get_api_resource(url)
            version_groups = self._move_generation_handler(res, **kwargs)
            generations = {vg for vg in version_groups}
            mv = {'name': name, 'generations': list(generations)}

            # Filter on generation if query field applied
            version_group = kwargs.get('version_group')
            if version_group:
                if version_group in mv['generations']:
                    yield mv
            else:
                yield mv

    def _move_generation_handler(self, res, **kwargs):
        """Parses generation reults and filters on any supplied queries.
        Yields a dict of the generation name.

        :param `res` - the moves generation result to parse
        :param `kwargs` - keyword arguments to filter results by
        :rtype dict
        :return generator of generation info
        """
        url = res['generation']['url']
        res = self._get_api_resource(url)
        for vg in res['version_groups']:
            yield vg['name']

    def _get_api_resource(self, url):
        """Retrives an API resource from a given url as JSON.

        :param url: the url resource to retrieve from the API
        :raises: `requests.exceptions.RequestException` if an error is
                encounterd during API request
        :rtype dict:
        :return dict of request's JSON response:
        """
        res = requests.get(url)
        res.raise_for_status()
        data = res.json()
        return data
