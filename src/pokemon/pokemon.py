import argparse
import itertools
import shutil
import sys

from pokemon import __version__ as version
from pokemon.api import PokeAPI, PokeError


def build_parser():
    """Returns a CLI parser for the PokeAPI using `argparse`.

    :param `argv` - a list of args to parse
    :rtype `argparse.ArgumentParser`
    :return `argparse.ArgumentParser` object
    """
    parser = argparse.ArgumentParser(prog='pokemon')
    parser.add_argument('--lookup',
                        help='pokemon name OR pokedex number to query moves for')
    parser.add_argument('--move-type',
                        help='move-type OR move-type id to query moves for')
    parser.add_argument('--generation',
                        help='generation to filter results by')
    parser.add_argument('--version',
                        action='version',
                        version=f"PokeCLI {version}",
                        help='version of PokeCLI program')
    return parser


# TODO: Move to helper module
def banner(s, c='='):
    """Returns a string with a underlined character banner.

    :param `s` - the string to underline
    :param `c` - the character to form the banner
    :rtype str
    :return string with banner
    """
    return f"{s}\n{c*len(s)}"


# TODO: Move to helper module
def align_row_cols(lcol, rcol, lfw_sp=0, rfw_sp=1, force_rfw_rcol=True):
    """Yields a string as a left/right adjusted row given the content
    of two associated columns (`lcol`, `rcol`, e.g. row1 = lcol[0] + rcol[0]).
    Format widths are determined by max len for each column.

    :param `lcol` - sequence of fields to be left adjusted
    :param `rcol` - sequence of fields to be right adjusted
    :param `lfw_sp` - space length of left format width
    :param `rfw_sp` - space length of right format width
    :param `force_rfw_rcol` - forces `rcol` content to be right adjusted
    :rtype str
    :return string of adjusted row
    """
    lfw = len(max(lcol, key=len))+lfw_sp
    rfw = len(max(rcol, key=len))+rfw_sp
    for left, right in zip(lcol, rcol):
        row = f"{left:<{lfw}}"
        if force_rfw_rcol:
            yield f"{row}{right:>{rfw}}"
        else:
            yield f"{row} {right}"


# TODO: Move to helper module
def truncate(s, char='...', limit=79):
    """Returns a truncated string to a given length limit. Attempts
    to account for sequences in truncating.

    :param `s` - the string to truncate
    :param `char` - the character string to truncate the result with
    :param `limit` - the character limit of the string to truncate
    :rtype str
    :return truncated string
    """
    # Account for truncating character length
    limit = limit - len(char)
    # Account for potential sequence
    limit = limit-1 if s[-1] in ('}', ')', ']') else limit
    # Truncate string
    return f"{s[:limit]}{char}{s[-1]}" if len(s) >= limit else s


def run(options):
    """Runs the PokeCLI program.

    :param `options` - parsed argparse CLI args
    """

    lookup = options.lookup
    move_type = options.move_type
    generation = options.generation

    p = PokeAPI()
    moves = None

    try:
        if lookup:
            # Handle case where lookup value is pokedex number
            try:
                lookup = int(lookup)
            except ValueError:
                pass

            # Get pokemon info
            poke_info = p.get_pokemon_info(lookup, version_group=generation)

            # Display pokemon name and pokedex number
            poke_fields = ("POKEMON:", "POKEDEX NUMBER:")
            poke_values = (poke_info['pokemon'],
                           str(poke_info['pokedex_number']))
            for line in align_row_cols(poke_fields, poke_values):
                print(line)
            print('-'*len(line))

            # Display pokemon moves filtered by generation
            moves = sorted(poke_info['moves'], key=lambda mv: mv['name'])

        if move_type:
            # Handle case where move type is id
            try:
                move_type = int(move_type)
            except ValueError:
                pass

            # Get move type info
            move_type_info = p.get_move_type_info(move_type,
                                                  version_group=generation)

            # Display move type name and id
            move_type_fields = ("MOVE TYPE:", "MOVE TYPE ID:")
            move_type_values = (move_type_info['move_type'],
                                str(move_type_info['move_type_id']))
            for line in align_row_cols(move_type_fields, move_type_values):
                print(line)
            print('-'*len(line))

            # NOTE: The PokeAPI returns the moves in descending order of their
            # pp field by default
            moves = list(itertools.islice(move_type_info['moves'], 10))

        # Display moves if any
        if moves is not None:
            title = f"{len(moves)} MOVE(S):"
            print()
            print(banner(title))

            # Display move name and associated generations
            move_names = [move['name'] for move in moves]
            move_generations = [f"- ({', '.join(move['generations'])})"
                                for move in moves]
            for line in align_row_cols(move_names,
                                       move_generations,
                                       force_rfw_rcol=False):
                terminal_width = shutil.get_terminal_size().columns
                print(truncate(f"\t{line}", limit=terminal_width-5))
    # Handle any errors
    except PokeError as err:
        print(repr(err))


def cli():
    # Build parser and parse CLI args
    parser = build_parser()
    args = parser.parse_args(sys.argv[1:])

    # Require at least one of lookup/move-type to be provided
    if args.lookup is None and args.move_type is None:
        parser.print_help()
    # Run Poke CLI
    else:
        run(args)


if __name__ == "__main__":  # pragma: no cover
    cli()
